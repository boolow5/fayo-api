package db

import (
	"fmt"

	"gopkg.in/mgo.v2"

	"bitbucket.org/boolow5/HMS/fayo-api/config"
)

var mongoSession *mgo.Session
var dbName string

func Init() {
	var err error
	config := config.Get()
	dbName = config.MgDBName
	mongoSession, err = mgo.Dial(config.MgDBURI)
	if err != nil {
		fmt.Println(config.MgDBURI)
		fmt.Println(config.MgDBName)
		panic(err)
	}
	mongoSession.SetMode(mgo.Monotonic, true)
}

func Get() *mgo.Database {
	if mongoSession == nil {
		return nil
	}
	cp := mongoSession.Copy()
	if cp != nil {
		return cp.DB(dbName)
	}
	return nil // mongoSession.Copy().DB(dbName)
}

func GetMongoSession() *mgo.Session {
	return mongoSession.Copy()
}

func Close() {
	mongoSession.Close()
}
