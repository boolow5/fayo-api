package models

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Appointment represents a patient
type Appointment struct {
	ID bson.ObjectId `json:"id" bson:"_id"`

	PatientID bson.ObjectId `json:"patient_id" bson:"patient_id"`
	DoctorID  bson.ObjectId `json:"doctor_id" bson:"doctor_id"`

	Notes string    `json:"notes" bson:"notes"`
	Date  time.Time `json:"date" bson:"date"`

	Timestamp `bson:",inline"`
}

// GetColName is used to implement DBObject
func (a Appointment) GetColName() string {
	return "appointment"
}

// GetID is used to implement DBObject
func (a Appointment) GetID() bson.ObjectId {
	return a.ID
}

// SetID is used to implement DBObject
func (a *Appointment) SetID(id bson.ObjectId) {
	a.ID = id
}

// SetTimestamp is used to implement DBObject
func (a *Appointment) SetTimestamp(isUpdate bool) {
	now := time.Now()
	a.CreatedAt = now
	if isUpdate {
		a.UpdatedAt = now
	}
}

// IsValid is used to implement DBObject
func (a Appointment) IsValid() (bool, []error) {
	if !bson.IsObjectIdHex(a.ID.Hex()) {
		return false, []error{fmt.Errorf("invalid diagnosis id")}
	}
	if !bson.IsObjectIdHex(a.PatientID.Hex()) {
		return false, []error{fmt.Errorf("invalid patient id")}
	}
	return true, nil
}

// AutoSetID is used to implement DBObject
func (a Appointment) AutoSetID() bool {
	return true
}
