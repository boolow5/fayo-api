package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/HMS/fayo-api/db"
	"bitbucket.org/boolow5/HMS/fayo-api/logger"
	"gopkg.in/mgo.v2/bson"
)

var (
	// Version of the last releases
	Version string
	// CommitNo last git commit number
	CommitNo string
	// LastBuidDate the last deployment date
	LastBuidDate string

	// DayTimeSchedules represents all times in a 24 hours
	DayTimeSchedules = []TimeSchedule{
		TimeSchedule{Name: "Morning", FromHour: 6, ToHour: 11},
		TimeSchedule{Name: "Noon", FromHour: 11, ToHour: 13},
		TimeSchedule{Name: "Afternoon", FromHour: 13, ToHour: 17},
		TimeSchedule{Name: "Evening", FromHour: 18, ToHour: 23},
		TimeSchedule{Name: "Midnight", FromHour: 0, ToHour: 1},
		TimeSchedule{Name: "Latenight", FromHour: 2, ToHour: 5},
	}
)

// DBObject defines the behavior of object that can be saved to database
type DBObject interface {
	GetColName() string
	GetID() bson.ObjectId
	SetID(bson.ObjectId)
	SetTimestamp(bool)
	IsValid() (bool, []error)
	AutoSetID() bool
}

// Timestamp is an object to be embedded in other objects for tracking date time
type Timestamp struct {
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
}

// TimeSchedule is a named time such as morning, afternoon, evening
type TimeSchedule struct {
	Name     string `json:"name" bson:"name"`
	FromHour int    `json:"from_hour" bson:"from_hour"`
	ToHour   int    `json:"to_hour" bson:"to_hour"`
}

// Save stores an item in the database
func Save(item DBObject) []error {
	if item.AutoSetID() {
		item.SetID(bson.NewObjectId())
	}
	logger.Debugf("Save(item: %s) ID: [%s]", item.GetColName(), item.GetID().Hex())
	if valid, errs := item.IsValid(); errs != nil || !valid {
		logger.Error("Errors:", errs)
		return append([]error{fmt.Errorf("%s is not valid", item.GetColName())}, errs...)
	}
	item.SetTimestamp(true)
	err := db.CreateStrong(item.GetColName(), item)
	if err != nil {
		return []error{err}
	}
	return nil
}

// Update stores changes to an item in the database
func Update(item DBObject) []error {
	item.SetTimestamp(false)
	if valid, errs := item.IsValid(); errs != nil || !valid {
		return append([]error{fmt.Errorf("%s is not valid", item.GetColName())}, errs...)
	}
	err := db.UpdateStrong(item.GetColName(), item.GetID(), item)
	if err != nil {
		return []error{err}
	}
	return nil
}

// Remove deletes an item from the database
func Remove(item DBObject) error {
	return db.DeleteId(item.GetColName(), item.GetID())
}

// GetItemByID finds DBObject item by it's ID
func GetItemByID(id bson.ObjectId, output DBObject) error {
	return db.FindOne(output.GetColName(), &bson.M{"_id": id}, output)
}

// GetItemsByFilter finds DBObject items by it's ID
func GetItemsByFilter(colName string, filter *bson.M, output []DBObject) error {
	return db.FindAll(colName, filter, output)
}

// GetItemByFilter finds DBObject item by it's ID
func GetItemByFilter(colName string, filter *bson.M, output DBObject) error {
	return db.FindOne(colName, filter, output)
}
