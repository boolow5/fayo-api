package models

import (
	"fmt"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Medicine represents a patient
type Medicine struct {
	ID            bson.ObjectId `json:"id" bson:"_id"`
	Name          string        `json:"name" bson:"name"`
	Company       string        `json:"company" bson:"company"`
	Country       string        `json:"country" bson:"country"`
	Cost          float64       `json:"cost" bson:"cost"`
	CostCurrency  string        `json:"cost_currency" bson:"cost_currency"`
	Price         float64       `json:"price" bson:"price"`
	PriceCurrency string        `json:"price_currency" bson:"price_currency"`

	QRCode `json:"qr_code" bson:"qr_code"`

	Timestamp `bson:",inline"`
}

// GetColName is used to implement DBObject
func (m Medicine) GetColName() string {
	return "medicine"
}

// GetID is used to implement DBObject
func (m Medicine) GetID() bson.ObjectId {
	return m.ID
}

// SetID is used to implement DBObject
func (m *Medicine) SetID(id bson.ObjectId) {
	m.ID = id
}

// SetTimestamp is used to implement DBObject
func (m *Medicine) SetTimestamp(isUpdate bool) {
	now := time.Now()
	m.CreatedAt = now
	if isUpdate {
		m.UpdatedAt = now
	}
}

// IsValid is used to implement DBObject
func (m Medicine) IsValid() (bool, []error) {
	if !bson.IsObjectIdHex(m.ID.Hex()) {
		return false, []error{fmt.Errorf("invalid medicine id")}
	}
	if strings.TrimSpace(m.Name) == "" {
		return false, []error{fmt.Errorf("invalid medicine name")}
	}
	return true, nil
}

// AutoSetID is used to implement DBObject
func (m Medicine) AutoSetID() bool {
	return true
}
