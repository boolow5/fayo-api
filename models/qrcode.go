package models

import (
	"fmt"
	"path/filepath"

	qrcode "github.com/skip2/go-qrcode"
	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/boolow5/HMS/fayo-api/config"
	"bitbucket.org/boolow5/HMS/fayo-api/logger"
	"bitbucket.org/boolow5/HMS/fayo-api/utils"
)

const (
	// QRTypeWallet is QR code that represents a wallet address
	QRTypeWallet = "wallet"
	// QRTypeUser is QR code that represents a user profile
	QRTypeUser = "user"
	// QRTypeVehicle is QR code that represents a vehicle
	QRTypeVehicle = "vehicle"
)

var (
	validQRTypes = []string{QRTypeUser}
)

// QRCode represents QR code data and is used for parsing and verifying it
type QRCode struct {
	RawURL     string     `json:"raw_url" bson:"raw_url"`
	TargetType string     `json:"target_type" bson:"target_type"`
	Response   QRResponse `json:"response" bson:"response"`
	Verified   bool       `json:"verified" bson:"verified"`
}

// QRResponse represents QRCode validation response
type QRResponse struct {
	UserID   string
	ItemID   string
	ItemType string
	Details  map[string]interface{}
}

// NewQRCode initializes QRCode
func NewQRCode(rawURL, qrType string, response QRResponse) QRCode {
	return QRCode{RawURL: rawURL, TargetType: qrType, Response: response}
}

// NewQRResponse initializes QRResponse
func NewQRResponse(userID, itemID, itemType string, details map[string]interface{}) QRResponse {
	return QRResponse{UserID: userID, ItemID: itemID, ItemType: itemType, Details: details}
}

// GetQRCodeFileName gets QRCode file name
func (qr QRCode) GetQRCodeFileName() string {
	fmt.Println("GetQRCodeFileName")
	if qr.Response.ItemID == "" {
		return ""
	}
	fileName := fmt.Sprintf("%s-%s.png", qr.TargetType, qr.Response.ItemID)
	return filepath.Join(config.GetStaticDirectory(), "qrcode", fileName)
}

// Valid checks if QRCode has all required data
func (qr QRCode) Valid() bool {
	return len(qr.RawURL) > 5 && utils.StringIn(qr.TargetType, validQRTypes, true)
}

// Verify checks if QR code and data includeda are all correct
func (qr *QRCode) Verify() error {
	// example url domain.com?userID=idstring&itemID=idstring&qrType=wallet
	// parse url and get user id, item id and qr type
	queries := utils.GetURLQueries(qr.RawURL)
	var userID, itemID, qrType string

	for i := 0; i < len(queries); i++ {
		if queries[i].Key == "userID" {
			userID = queries[i].Value
		} else if queries[i].Key == "itemID" {
			itemID = queries[i].Value
		} else if queries[i].Key == "qrType" {
			qrType = queries[i].Value
		}
	}
	if len(userID) != 24 {
		return fmt.Errorf("missing userID")
	}
	if len(itemID) != 24 {
		return fmt.Errorf("missing itemID")
	}
	if !utils.StringIn(qrType, validQRTypes, false) {
		return fmt.Errorf("missing qrType")
	}
	// check if user exist
	var user User
	err := GetItemByID(bson.ObjectIdHex(userID), &user)
	if err != nil {
		logger.Errorf("failed to get user: %v", err)
		return fmt.Errorf("invalid user id")
	}
	qr.TargetType = qrType
	// check if item exists and belongs to the user above
	switch qr.TargetType {
	case QRTypeUser:
		if itemID != user.ID.Hex() {
			return fmt.Errorf("invalid user profile id")
		}
		qr.Response = NewQRResponse(user.ID.Hex(), user.ID.Hex(), QRTypeUser, user.Profile.ToMap())

	default:
		return fmt.Errorf("Invalid QR Code type '%s'", qrType)
	}
	return nil
}

// Generate creates QR code file and saves them to file system
func (qr *QRCode) Generate() (filePath string, err error) {
	if !qr.Valid() {
		if len(qr.RawURL) < 6 {
			return "", fmt.Errorf("QR code URL is required")
		} else if !utils.StringIn(qr.TargetType, []string{QRTypeWallet, QRTypeUser, QRTypeVehicle}, true) {
			return "", fmt.Errorf("QR code type is not recognized")
		} else {
			return "", fmt.Errorf("unknown error occurred")
		}
	}
	err = qr.Verify()
	if err != nil {
		return "", err
	}
	staticPath := config.GetStaticDirectory()
	fileName := fmt.Sprintf("%s-%s.png", qr.TargetType, qr.Response.ItemID)
	fullPath := filepath.Join(staticPath, "qrcode", fileName)
	err = qrcode.WriteFile(qr.RawURL, qrcode.Medium, 256, fullPath)
	if err != nil {
		return fullPath, err
	}
	return fullPath, nil
}
