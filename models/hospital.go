package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/HMS/fayo-api/db"

	"gopkg.in/mgo.v2/bson"
)

// Hospital represents an hospital
type Hospital struct {
	ID      bson.ObjectId `json:"id" bson:"_id"`
	Name    string        `json:"name" bson:"name"`
	Slogan  string        `json:"slogan" bson:"slogan"`
	Address string        `json:"address" bson:"address"`

	Contacts []string        `json:"contacts" bson:"contacts"`
	UserIDs  []bson.ObjectId `json:"user_id" bson:"user_id"`

	Departments []bson.ObjectId `json:"departments" bson:"departments"`

	Timestamp `bson:",inline"`
}

// GetColName is used to implement DBObject
func (h Hospital) GetColName() string {
	return "hospital"
}

// GetID is used to implement DBObject
func (h Hospital) GetID() bson.ObjectId {
	return h.ID
}

// SetID is used to implement DBObject
func (h *Hospital) SetID(id bson.ObjectId) {
	h.ID = id
}

// SetTimestamp is used to implement DBObject
func (h *Hospital) SetTimestamp(isUpdate bool) {
	now := time.Now()
	h.CreatedAt = now
	if isUpdate {
		h.UpdatedAt = now
	}
}

// IsValid is used to implement DBObject
func (h *Hospital) IsValid() (bool, []error) {
	if !bson.IsObjectIdHex(h.ID.Hex()) {
		return false, []error{fmt.Errorf("invalid hospital id")}
	}
	// should have at least one user ID of the owner
	var userID bson.ObjectId
	if len(h.UserIDs) > 0 {
		userID = h.UserIDs[0]
	}
	if bson.IsObjectIdHex(userID.Hex()) {
		fmt.Println("Fetch hospital user")
		var user User
		err := GetItemByID(userID, &user)
		if db.IsNotFound(err) {
			return false, []error{fmt.Errorf("user does not exist")}
		} else if err != nil {
			return false, []error{err}
		}
	} else if !bson.IsObjectIdHex(userID.Hex()) {
		return false, []error{fmt.Errorf("invalid hospital user id")}
	}
	return true, nil
}

// AutoSetID is used to implement DBObject
func (h Hospital) AutoSetID() bool {
	return true
}
