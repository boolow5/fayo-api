package models

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Diagnosis represents a patient
type Diagnosis struct {
	ID bson.ObjectId `json:"id" bson:"_id"`

	PatientID        bson.ObjectId `json:"patient_id" bson:"patient_id"`
	DoctorID         bson.ObjectId `json:"doctor_id" bson:"doctor_id"`
	SignsAndSymptoms string        `json:"notes" bson:"notes"`

	Timestamp `bson:",inline"`
}

// GetColName is used to implement DBObject
func (d Diagnosis) GetColName() string {
	return "diagnosis"
}

// GetID is used to implement DBObject
func (d Diagnosis) GetID() bson.ObjectId {
	return d.ID
}

// SetID is used to implement DBObject
func (d *Diagnosis) SetID(id bson.ObjectId) {
	d.ID = id
}

// SetTimestamp is used to implement DBObject
func (d *Diagnosis) SetTimestamp(isUpdate bool) {
	now := time.Now()
	d.CreatedAt = now
	if isUpdate {
		d.UpdatedAt = now
	}
}

// IsValid is used to implement DBObject
func (d Diagnosis) IsValid() (bool, []error) {
	if !bson.IsObjectIdHex(d.ID.Hex()) {
		return false, []error{fmt.Errorf("invalid diagnosis id")}
	}
	if !bson.IsObjectIdHex(d.PatientID.Hex()) {
		return false, []error{fmt.Errorf("invalid patient id")}
	}
	if len(d.SignsAndSymptoms) < 1 {
		return false, []error{fmt.Errorf("empty signs and symptoms")}
	}
	return true, nil
}

// AutoSetID is used to implement DBObject
func (d Diagnosis) AutoSetID() bool {
	return true
}
