package models

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Prescription represents a patient
type Prescription struct {
	ID bson.ObjectId `json:"id" bson:"_id"`

	PatientID   bson.ObjectId `json:"patient_id" bson:"patient_id"`
	DoctorID    bson.ObjectId `json:"doctor_id" bson:"doctor_id"`
	Medications []Medication  `json:"medications" bson:"medications"`
	Notes       string        `json:"notes" bson:"notes"`
	Date        time.Time     `json:"date" bson:"date"`

	Timestamp `bson:",inline"`
}

// GetColName is used to implement DBObject
func (p Prescription) GetColName() string {
	return "prescription"
}

// GetID is used to implement DBObject
func (p Prescription) GetID() bson.ObjectId {
	return p.ID
}

// SetID is used to implement DBObject
func (p *Prescription) SetID(id bson.ObjectId) {
	p.ID = id
}

// SetTimestamp is used to implement DBObject
func (p *Prescription) SetTimestamp(isUpdate bool) {
	now := time.Now()
	p.CreatedAt = now
	if isUpdate {
		p.UpdatedAt = now
	}
}

// IsValid is used to implement DBObject
func (p Prescription) IsValid() (bool, []error) {
	if !bson.IsObjectIdHex(p.ID.Hex()) {
		return false, []error{fmt.Errorf("invalid diagnosis id")}
	}
	if !bson.IsObjectIdHex(p.PatientID.Hex()) {
		return false, []error{fmt.Errorf("invalid patient id")}
	}
	return true, nil
}

// AutoSetID is used to implement DBObject
func (p Prescription) AutoSetID() bool {
	return true
}
