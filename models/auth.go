package models

import (
	"fmt"
	"path/filepath"
	"time"

	"bitbucket.org/boolow5/HMS/fayo-api/config"
	"bitbucket.org/boolow5/HMS/fayo-api/db"
	"bitbucket.org/boolow5/HMS/fayo-api/logger"
	"bitbucket.org/boolow5/HMS/fayo-api/utils"
	jwt "gopkg.in/dgrijalva/jwt-go.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	// UserGenderFemale represents women
	UserGenderFemale = "female"
	// UserGenderMale represents men
	UserGenderMale = "male"
	// UserNeutralGender represents those outside the two genders
	UserNeutralGender = "neutral"
)

const (
	// GuestUserLevel is for access control
	GuestUserLevel = iota
	// NormalUserLevel is for access control
	NormalUserLevel
	// AdminUserLevel is for access control
	AdminUserLevel
)

// User is user's authentication info
type User struct {
	ID       bson.ObjectId `json:"id" bson:"_id"`
	Email    string        `json:"email" bson:"email"`
	Phone    string        `json:"phone" bson:"phone"`
	Password string        `json:"password" bson:"password"`
	Level    int           `json:"level" bson:"level"`
	Profile  Profile       `json:"profile" bson:"profile"`

	Timestamp `bson:",inline"`
}

// Profile is user's personal info
type Profile struct {
	UserID     bson.ObjectId `json:"id" bson:"_id"`
	FirstName  string        `json:"first_name" bson:"first_name"`
	MiddleName string        `json:"middle_name" bson:"middle_name"`
	LastName   string        `json:"last_name" bson:"last_name"`
	Gender     string        `json:"gender" bson:"gender"`
	Birthday   time.Time     `json:"birthday" bson:"birthday"`
	ProfilePic string        `json:"profile_pic" bson:"profile_pic"`
}

// ToMap changes Profile to map
func (p Profile) ToMap() map[string]interface{} {
	return map[string]interface{}{
		"user_id":     p.UserID.Hex(),
		"first_name":  p.FirstName,
		"last_name":   p.LastName,
		"middle_name": p.MiddleName,
		"gender":      p.Gender,
		"birthday":    p.Birthday,
		"profile_pic": p.ProfilePic,
	}
}

// GenerateURL produces profile url for generating QR code etc.
func (p Profile) GenerateURL(domain string) string {
	return fmt.Sprintf("%s/qrcode?userID=%s&itemID=%s&qrType=%s", domain, p.UserID.Hex(), p.UserID.Hex(), QRTypeUser)
}

// GetQRCodeFileName gets profile QRCode file name
func (p Profile) GetQRCodeFileName() string {
	fileName := fmt.Sprintf("%s-%s.png", QRTypeUser, p.UserID.Hex())
	return filepath.Join(config.GetStaticDirectory(), "qrcode", fileName)
}

// FullName returns user's first, middle and last name
func (p Profile) FullName() string {
	return fmt.Sprintf("%s %s %s", p.FirstName, p.MiddleName, p.LastName)
}

// GetUserByEmail finds a user by his/her email
func GetUserByEmail(email string) (user User, err error) {
	logger.Debugf("GetUserByEmail(email: %s)", email)
	err = db.FindOne(UserCollection, &bson.M{"email": email}, &user)
	if err != nil {
		return user, err
	}
	return user, nil
}

// GetUserByPhone finds a user by his/her phone
func GetUserByPhone(phone string) (user User, err error) {
	logger.Debugf("GetUserByPhone(phone: %s)", phone)
	err = db.FindOne(UserCollection, &bson.M{"phone": phone}, &user)
	if err != nil {
		return user, err
	}
	return user, nil
}

// GetColName satisfies DBObject interface
func (u User) GetColName() string {
	return UserCollection
}

// GetID satisfies DBObject interface
func (u User) GetID() bson.ObjectId {
	return u.ID
}

// SetID satisfies DBObject interface
func (u *User) SetID(id bson.ObjectId) {
	u.ID = id
	u.Profile.UserID = id
	fmt.Printf("SetID id=%s, profile %s\n", u.ID.Hex(), u.Profile.UserID.Hex())
	if len(u.Password) < 50 {
		u.SetPassword(u.Password)
	}
}

// SetTimestamp satisfies DBObject interface
func (u *User) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		u.CreatedAt = now
		u.UpdatedAt = now
	} else {
		u.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (u User) IsValid() (valid bool, errs []error) {
	emailIsValid := utils.IsValidEmail(u.Email)
	phoneIsValid := utils.IsValidPhone(u.Phone)
	if !emailIsValid && !phoneIsValid {
		if !emailIsValid {
			errs = append(errs, []error{fmt.Errorf("email")}...)
		} else if !phoneIsValid {
			errs = append(errs, []error{fmt.Errorf("phone")}...)
		}
	}
	if len(u.Profile.UserID.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("profile user id")}...)
	}
	if u.Profile.FirstName == "" {
		errs = append(errs, []error{fmt.Errorf("first name")}...)
	}
	return true, errs
}

// AutoSetID satisfies DBObject interface, to control setting item ID behavior
func (u User) AutoSetID() bool {
	return true
}

// SetPassword sets and hashes password of a user object
func (u *User) SetPassword(newPassword string) (err error) {
	newPassword, err = utils.BcryptHash(newPassword)
	if err == nil {
		u.Password = newPassword
		return nil
	}
	return NewError(HashingError, err.Error())
}

// GetToken creates JWT Token
func (u *User) GetToken() (string, error) {
	if len(u.ID.Hex()) != 24 {
		return "", NewError(ValidationError, "invalid user id")
	}
	token := jwt.New(jwt.SigningMethodHS256)

	token.Claims["id"] = u.ID.Hex()

	tokenStr, err := token.SignedString([]byte(config.Get().JWTKey))
	return tokenStr, err
}

// Authenticate checks if current user matches the hashed password stored in the database
func (u User) Authenticate(pass string) (bool, error) {
	fmt.Printf("Authenticate(pass: %s) PASS: %s", pass, u.Password)
	if !utils.IsValidEmail(u.Email) && !utils.IsValidPhone(u.Phone) {
		return false, NewError(AuthenticationError, "Invalid email or phone")
	}
	authenticated := utils.AuthenticatePassword(u.Password, pass)
	fmt.Println("authenticated:", authenticated)
	if authenticated {
		return true, nil
	}
	if len(u.Phone) == 0 && len(u.Email) == 0 {
		return false, NewError(AuthenticationError, "Email/phone and password do not match")
	} else if len(u.Email) == 0 {
		return false, NewError(AuthenticationError, "Email and password do not match")
	}
	return false, NewError(AuthenticationError, "Phone and password do not match")
}
