package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/HMS/fayo-api/db"
	"gopkg.in/mgo.v2/bson"
)

// Patient represents a patient
type Patient struct {
	ID     bson.ObjectId `json:"id" bson:"_id"`
	UserID bson.ObjectId `json:"user_id" bson:"user_id"`
	Age    int           `json:"age" bson:"age"`

	Type   int `json:"type" bson:"type"`
	QRCode `json:"qr_code" bson:"qr_code"`

	Timestamp `bson:",inline"`
}

// GetColName is used to implement DBObject
func (p Patient) GetColName() string {
	return "patient"
}

// GetID is used to implement DBObject
func (p Patient) GetID() bson.ObjectId {
	return p.ID
}

// SetID is used to implement DBObject
func (p *Patient) SetID(id bson.ObjectId) {
	p.ID = id
}

// SetTimestamp is used to implement DBObject
func (p *Patient) SetTimestamp(isUpdate bool) {
	now := time.Now()
	p.CreatedAt = now
	if isUpdate {
		p.UpdatedAt = now
	}
}

// IsValid is used to implement DBObject
func (p *Patient) IsValid() (bool, []error) {
	if !bson.IsObjectIdHex(p.ID.Hex()) {
		return false, []error{fmt.Errorf("invalid patient id")}
	}
	if bson.IsObjectIdHex(p.UserID.Hex()) {
		var user User
		err := GetItemByID(p.UserID, &user)
		fmt.Printf("User: %+v\n", p)
		if db.IsNotFound(err) {
			return false, []error{fmt.Errorf("user does not exist")}
		} else if err != nil {
			return false, []error{err}
		}
	} else if !bson.IsObjectIdHex(p.UserID.Hex()) {
		return false, []error{fmt.Errorf("invalid patient user id")}
	}
	return true, nil
}

// AutoSetID is used to implement DBObject
func (p Patient) AutoSetID() bool {
	return true
}
