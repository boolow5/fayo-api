package models

import (
	"gopkg.in/mgo.v2/bson"
)

// Medication represents a patient
type Medication struct {
	MedicineID  bson.ObjectId  `json:"medicine_id" bson:"medicine_id,omitempty"`
	Notes       string         `json:"notes" bson:"notes"`
	Dosage      string         `json:"dose" bson:"dose"`
	TimesPerDay []TimeSchedule `json:"times_per_day" bson:"times_per_day"`
}
