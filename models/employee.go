package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/HMS/fayo-api/db"

	"gopkg.in/mgo.v2/bson"
)

// Employee represents an employee
type Employee struct {
	ID     bson.ObjectId `json:"id" bson:"_id"`
	UserID bson.ObjectId `json:"user_id" bson:"user_id"`

	Type   int `json:"type" bson:"type"`
	QRCode `json:"qr_code" bson:"qr_code"`

	Timestamp `bson:",inline"`
}

// GetColName is used to implement DBObject
func (e Employee) GetColName() string {
	return "employee"
}

// GetID is used to implement DBObject
func (e Employee) GetID() bson.ObjectId {
	return e.ID
}

// SetID is used to implement DBObject
func (e *Employee) SetID(id bson.ObjectId) {
	e.ID = id
}

// SetTimestamp is used to implement DBObject
func (e *Employee) SetTimestamp(isUpdate bool) {
	now := time.Now()
	e.CreatedAt = now
	if isUpdate {
		e.UpdatedAt = now
	}
}

// IsValid is used to implement DBObject
func (e *Employee) IsValid() (bool, []error) {
	if !bson.IsObjectIdHex(e.ID.Hex()) {
		return false, []error{fmt.Errorf("invalid employee id")}
	}
	if bson.IsObjectIdHex(e.UserID.Hex()) {
		fmt.Println("Fetch employee user")
		var user User
		err := GetItemByID(e.UserID, &user)
		if db.IsNotFound(err) {
			return false, []error{fmt.Errorf("user does not exist")}
		} else if err != nil {
			return false, []error{err}
		}
	} else if !bson.IsObjectIdHex(e.UserID.Hex()) {
		return false, []error{fmt.Errorf("invalid employee user id")}
	}
	return true, nil
}

// AutoSetID is used to implement DBObject
func (e Employee) AutoSetID() bool {
	return true
}
