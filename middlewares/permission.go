package middlewares

import (
	"errors"
	"fmt"

	"bitbucket.org/boolow5/HMS/fayo-api/db"
	"bitbucket.org/boolow5/HMS/fayo-api/models"
	"bitbucket.org/boolow5/HMS/fayo-api/utils"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

type perm struct {
	Level int
	Paths []string
}

var permissions = []perm{
	perm{
		Level: models.GuestUserLevel,
		Paths: []string{},
	},
	perm{
		Level: models.NormalUserLevel,
		Paths: []string{},
	},
	perm{
		Level: models.AdminUserLevel,
		Paths: []string{
			"POST /api/v1/employee",
			"PUT /api/v1/employee",
			"DELETE /api/v1/employee",

			"POST /api/v1/patient",
			"PUT /api/v1/patient",
			"DELETE /api/v1/patient",
		},
	},
}

// Permission middleware
func Permission() gin.HandlerFunc {
	return func(c *gin.Context) {
		userID, OK := c.MustGet("id").(string)
		if !OK {
			fmt.Println("IS NOT OK")
			utils.AbortWithError(c, 401, errors.New("Access denied: authentication error"))
			return
		}
		user := struct {
			Level int `bson:"level"`
		}{-1}
		if bson.IsObjectIdHex(userID) {
			err := db.FindOne(models.UserCollection, &bson.M{"_id": bson.ObjectIdHex(userID)}, &user)
			if err == nil {
				c.Set("level", user.Level)
			}
		}
		level := findLevelByPath(c.Request.Method, c.Request.RequestURI)
		if user.Level < level {
			utils.AbortWithError(c, 401, errors.New("Access denied: permission denied"))
			return
		}
		fmt.Printf("[Permission] [%d] %s %s\n", user.Level, c.Request.Method, c.Request.RequestURI)
	}
}

func findLevelByPath(method, path string) int {
	fmt.Printf("findLevelByPath: %s\n", fmt.Sprintf("%s %s", method, path))
	for _, perm := range permissions {
		for _, url := range perm.Paths {
			if fmt.Sprintf("%s %s", method, path) == url {
				return perm.Level
			}
		}
	}
	return -1
}
