package middlewares

import (
	"errors"
	"fmt"
	"strings"

	"bitbucket.org/boolow5/HMS/fayo-api/config"
	"bitbucket.org/boolow5/HMS/fayo-api/utils"
	"github.com/gin-gonic/gin"
	jwt "gopkg.in/dgrijalva/jwt-go.v2"
)

// Auth is middleware that handles verification and checking of JWT header
func Auth() gin.HandlerFunc {
	return func(c *gin.Context) {
		config := config.Get()
		var authHeader []string
		var present bool
		if authHeader, present = c.Request.Header["Authorization"]; !present && len(authHeader) < 1 {
			utils.AbortWithError(c, 401, errors.New("Access denied: authorization header not found"))
			return
		}

		if strings.HasPrefix(authHeader[0], "Bearer ") {
			authHeader[0] = strings.Replace(authHeader[0], "Bearer ", "", 1)
		}

		token, err := jwt.Parse(authHeader[0], func(token *jwt.Token) (interface{}, error) {
			return []byte(config.JWTKey), nil
		})

		if err != nil || !token.Valid {
			utils.AbortWithError(c, 401, err)
			return
		}

		// tokenData, err := validateToken(token.Claims)
		// validate
		id, ok := token.Claims["id"].(string)
		if !ok {
			err = fmt.Errorf(`Cannot find or convert key "id" from token`)
			utils.AbortWithError(c, 401, err)
			return
		}

		if err != nil {
			utils.AbortWithError(c, 401, err)
			return
		}
		c.Set("id", id)
	}
}
