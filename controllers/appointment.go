package controllers

import (
	"net/http"

	"bitbucket.org/boolow5/HMS/fayo-api/models"
	"github.com/gin-gonic/gin"
)

// CreateAppointment handles appointment creation
func CreateAppointment(c *gin.Context) {
	var appointment models.Appointment
	errs := CreateDBObject(c, &appointment)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"appointment": appointment})
}

// UpdateAppointment handles appointment update
func UpdateAppointment(c *gin.Context) {
	var appointment models.Appointment
	errs := UpdateDBObject(c, &appointment)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"appointment": appointment})
}

// DeleteAppointment handles appointment update
func DeleteAppointment(c *gin.Context) {
	var appointment models.Appointment
	errs := DeleteDBObject(c, &appointment)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"msg": "success"})
}
