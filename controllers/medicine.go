package controllers

import (
	"net/http"

	"bitbucket.org/boolow5/HMS/fayo-api/models"
	"github.com/gin-gonic/gin"
)

// CreateMedicine handles medicine creation
func CreateMedicine(c *gin.Context) {
	var medicine models.Medicine
	errs := CreateDBObject(c, &medicine)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"medicine": medicine})
}

// UpdateMedicine handles medicine update
func UpdateMedicine(c *gin.Context) {
	var medicine models.Medicine
	errs := UpdateDBObject(c, &medicine)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"medicine": medicine})
}

// DeleteMedicine handles medicine update
func DeleteMedicine(c *gin.Context) {
	var medicine models.Medicine
	errs := DeleteDBObject(c, &medicine)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"msg": "success"})
}
