package controllers

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/boolow5/HMS/fayo-api/logger"
	"bitbucket.org/boolow5/HMS/fayo-api/notifications"
	"github.com/gin-gonic/gin"
)

// Ping is used to test sse channel is working
func Ping(c *gin.Context) {
	channel := c.Query("channel")
	msg := []byte(fmt.Sprintf("[%s]\tPONG!", time.Now().Format(time.ANSIC)))
	targets := []string{"sse"}
	options := map[string]interface{}{
		"channel": channel,
	}
	err := notifications.Send(msg, targets, options)
	if err != nil {
		logger.Error("failed to send PONG message:", err)
		AbortWithError(c, http.StatusBadRequest, NewError(UknownError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, successResponse)
}
