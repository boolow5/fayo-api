package controllers

import (
	"bitbucket.org/boolow5/HMS/fayo-api/models"
	"github.com/gin-gonic/gin"
)

// CreateDBObject is generic method that gets object from context and saves to the database
func CreateDBObject(c *gin.Context, output models.DBObject) []error {
	err := c.Bind(output)
	if err != nil {
		return []error{err}
	}
	errs := models.Save(output)
	if len(errs) > 0 {
		return errs
	}
	return nil
}

// UpdateDBObject is generic method that gets object from context and saves changes to the database
func UpdateDBObject(c *gin.Context, output models.DBObject) []error {
	err := c.Bind(output)
	if err != nil {
		return []error{err}
	}
	errs := models.Update(output)
	if len(errs) > 0 {
		return errs
	}
	return nil
}

// DeleteDBObject is generic method that gets object from context and saves changes to the database
func DeleteDBObject(c *gin.Context, output models.DBObject) []error {
	err := c.Bind(output)
	if err != nil {
		return []error{err}
	}
	err = models.Remove(output)
	if err != nil {
		return []error{err}
	}
	return nil
}
