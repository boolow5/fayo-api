package controllers

import (
	"net/http"

	"bitbucket.org/boolow5/HMS/fayo-api/models"
	"github.com/gin-gonic/gin"
)

// CreatePrescription handles prescription creation
func CreatePrescription(c *gin.Context) {
	var prescription models.Prescription
	errs := CreateDBObject(c, &prescription)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"prescription": prescription})
}

// UpdatePrescription handles prescription update
func UpdatePrescription(c *gin.Context) {
	var prescription models.Prescription
	errs := UpdateDBObject(c, &prescription)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"prescription": prescription})
}

// DeletePrescription handles prescription update
func DeletePrescription(c *gin.Context) {
	var prescription models.Prescription
	errs := DeleteDBObject(c, &prescription)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"msg": "success"})
}
