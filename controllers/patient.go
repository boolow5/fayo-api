package controllers

import (
	"net/http"

	"bitbucket.org/boolow5/HMS/fayo-api/models"
	"github.com/gin-gonic/gin"
)

// CreatePatient handles patient creation
func CreatePatient(c *gin.Context) {
	var patient models.Patient
	errs := CreateDBObject(c, &patient)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"patient": patient})
}

// UpdatePatient handles patient update
func UpdatePatient(c *gin.Context) {
	var patient models.Patient
	errs := UpdateDBObject(c, &patient)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"patient": patient})
}

// DeletePatient handles patient update
func DeletePatient(c *gin.Context) {
	var patient models.Patient
	errs := DeleteDBObject(c, &patient)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"msg": "success"})
}
