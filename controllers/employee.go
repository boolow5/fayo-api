package controllers

import (
	"net/http"

	"bitbucket.org/boolow5/HMS/fayo-api/models"
	"github.com/gin-gonic/gin"
)

// CreateEmployee handles employee creation
func CreateEmployee(c *gin.Context) {
	var employee models.Employee
	errs := CreateDBObject(c, &employee)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"employee": employee})
}

// UpdateEmployee handles employee update
func UpdateEmployee(c *gin.Context) {
	var employee models.Employee
	errs := UpdateDBObject(c, &employee)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"employee": employee})
}

// DeleteEmployee handles employee update
func DeleteEmployee(c *gin.Context) {
	var employee models.Employee
	errs := DeleteDBObject(c, &employee)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"msg": "success"})
}
