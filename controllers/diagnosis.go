package controllers

import (
	"net/http"

	"bitbucket.org/boolow5/HMS/fayo-api/models"
	"github.com/gin-gonic/gin"
)

// CreateDiagnosis handles diagnosis creation
func CreateDiagnosis(c *gin.Context) {
	var diagnosis models.Diagnosis
	errs := CreateDBObject(c, &diagnosis)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"diagnosis": diagnosis})
}

// UpdateDiagnosis handles diagnosis update
func UpdateDiagnosis(c *gin.Context) {
	var diagnosis models.Diagnosis
	errs := UpdateDBObject(c, &diagnosis)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"diagnosis": diagnosis})
}

// DeleteDiagnosis handles diagnosis update
func DeleteDiagnosis(c *gin.Context) {
	var diagnosis models.Diagnosis
	errs := DeleteDBObject(c, &diagnosis)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"msg": "success"})
}
