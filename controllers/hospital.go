package controllers

import (
	"net/http"

	"bitbucket.org/boolow5/HMS/fayo-api/models"
	"github.com/gin-gonic/gin"
)

// CreateHospital handles hospital creation
func CreateHospital(c *gin.Context) {
	var hospital models.Hospital
	errs := CreateDBObject(c, &hospital)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"hospital": hospital})
}

// UpdateHospital handles hospital update
func UpdateHospital(c *gin.Context) {
	var hospital models.Hospital
	errs := UpdateDBObject(c, &hospital)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"hospital": hospital})
}

// DeleteHospital handles hospital update
func DeleteHospital(c *gin.Context) {
	var hospital models.Hospital
	errs := DeleteDBObject(c, &hospital)
	if len(errs) > 0 {
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(http.StatusBadRequest, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"msg": "success"})
}
