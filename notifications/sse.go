package notifications

import (
	"fmt"

	"bitbucket.org/boolow5/HMS/fayo-api/logger"
	"github.com/mroth/sseserver"
)

// SSEServer is SSE server that handles SSE notifications
var SSEServer *sseserver.Server

// Start SSE server
func Start(port string) {
	SSEServer = sseserver.NewServer()

	SSEServer.Serve(port)
}

// Broadcast sends data to namespace channel
func Broadcast(event, namespace string, data []byte) {
	logger.Debugf("Broadcast(event: '%s', namespace: '%s', data: '%s')", event, namespace, string(data))
	SSEServer.Broadcast <- sseserver.SSEMessage{Event: event, Data: data, Namespace: namespace}
	logger.Debug("SSE SENT!")
}

// Send is used to broadcast notification to different targets
// TODO: now it accepts only SSE but later it should support email and other types
func Send(msg []byte, targets []string, options map[string]interface{}) error {
	for _, target := range targets {
		if target == "sse" {
			channel, ok := options["channel"].(string)
			if ok && channel != "" {
				Broadcast("", channel, msg)
			} else {
				return fmt.Errorf("SSE message failed [OK:%v] channel: '%v'", ok, channel)
			}
		}
	}
	return nil
}
