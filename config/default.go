package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

var (
	config   Configuration
	confPath string
)

// Configuration holds all config data for the project
type Configuration struct {
	Version         string        `json:"version"`
	MgDBName        string        `json:"MgDBName"`
	MgDBURI         string        `json:"MgDBURI"`
	JWTKey          string        `json:"JWTKey"`
	Port            string        `json:"Port"`
	SSEPort         string        `json:"SSEPort"`
	MapKey          string        `json:"MapKey"`
	Admin           Admin         `json:"admin"`
	MailGun         MailGunConfig `json:"MailGun"`
	StaticDirectory string        `json:"static_directory"`
	StaticDomain    string        `json:"static_domain"`
}

// Admin is initial admin that will have full access to everything
type Admin struct {
	Email     string `json:"email"`
	Password  string `json:"password"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

// MailGunConfig is used for connecting to mail gun API
type MailGunConfig struct {
	APIKey    string `json:"APIKey"`
	DomainKey string `json:"DomainKey"`
	Email     string `json:"Email"`
}

func init() {
	if len(os.Args) > 1 {
		confPath = os.Args[1]
	}
	if err := Load(confPath); err != nil {
		fmt.Println("Cannot load config file reason:", err)
		panic(err)
	}
	fmt.Println("Initialized configuration.")
}

// Get returns config local variable
func Get() Configuration {
	return config
}

// Load reads the config data from the json file
func Load(confPath string) error {
	file, err := ioutil.ReadFile(filepath.Join(confPath, "config.json"))
	if err != nil {
		return err
	}
	return json.Unmarshal(file, &config)
}

// GetPath returns directory which contains the config.json file
func GetPath() string {
	return confPath
}

// GetStaticDirectory full path for static files
func GetStaticDirectory() string {
	if os.Getenv("QAADO_DEV") != "true" {
		return config.StaticDirectory
	}
	return "static"
}
