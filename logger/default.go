package logger

import (
	"github.com/sirupsen/logrus"
)

const (
	// DebugLevel is the logger level
	DebugLevel = iota
	// InfoLevel is the logger level
	InfoLevel
	// WarnLevel is the logger level
	WarnLevel
	// ErrorLevel is the logger level
	ErrorLevel
	// FatalLevel is the logger level
	FatalLevel
	// PanicLevel is the logger level
	PanicLevel
)

var log *logrus.Logger

// Init sets the level
func Init(level int) {
	log = logrus.New()

	switch level {
	case DebugLevel:
		log.SetLevel(logrus.DebugLevel)
	case InfoLevel:
		log.SetLevel(logrus.InfoLevel)
	case WarnLevel:
		log.SetLevel(logrus.WarnLevel)
	case ErrorLevel:
		log.SetLevel(logrus.ErrorLevel)
	case FatalLevel:
		log.SetLevel(logrus.FatalLevel)
	case PanicLevel:
		log.SetLevel(logrus.PanicLevel)
	}
}

// Get returns unexported log
func Get() *logrus.Logger {
	return log
}

// Debug is wrapper for logrus.Logger.Debug
func Debug(args ...interface{}) {
	log.Debug(args...)
}

// Debugf is wrapper for logrus.Logger.Debugf
func Debugf(format string, args ...interface{}) {
	log.Debugf(format, args...)
}

// Info is wrapper for logrus.Logger.Info
func Info(args ...interface{}) {
	log.Info(args...)
}

// Infof is wrapper for logrus.Logger.Infof
func Infof(format string, args ...interface{}) {
	log.Infof(format, args...)
}

// Panic is wrapper for logrus.Logger.Panic
func Panic(args ...interface{}) {
	log.Panic(args...)
}

// Panicf is wrapper for logrus.Logger.Panicf
func Panicf(format string, args ...interface{}) {
	log.Panicf(format, args...)
}

// Warn is wrapper for logrus.Logger.Warn
func Warn(args ...interface{}) {
	log.Warn(args...)
}

// Warnf is wrapper for logrus.Logger.Warnf
func Warnf(format string, args ...interface{}) {
	log.Warnf(format, args...)
}

// Error is wrapper for logrus.Logger.Error
func Error(args ...interface{}) {
	log.Error(args...)
}

// Errorf is wrapper for logrus.Logger.Errorf
func Errorf(format string, args ...interface{}) {
	log.Errorf(format, args...)
}
