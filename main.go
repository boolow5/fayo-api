package main

import (
	"fmt"
	"os"

	"bitbucket.org/boolow5/HMS/fayo-api/config"
	"bitbucket.org/boolow5/HMS/fayo-api/db"
	"bitbucket.org/boolow5/HMS/fayo-api/logger"
	"bitbucket.org/boolow5/HMS/fayo-api/models"
	"bitbucket.org/boolow5/HMS/fayo-api/notifications"
	"bitbucket.org/boolow5/HMS/fayo-api/router"
)

func main() {
	logger.Init(logger.DebugLevel)
	models.Version = config.Get().Version
	fmt.Printf("Starting %s carpooling api...\n", os.Args[0])
	db.Init()
	err := models.Init()
	if err != nil {
		panic(err)
	}
	router := router.Init()
	conf := config.Get()
	go notifications.Start(conf.SSEPort)
	notifications.InitMailGun(conf.MailGun)
	router.Run(conf.Port)
}
