package router

import (
	"net/http"

	"bitbucket.org/boolow5/HMS/fayo-api/controllers"
	"bitbucket.org/boolow5/HMS/fayo-api/middlewares"
	"bitbucket.org/boolow5/HMS/fayo-api/models"
	"github.com/gin-gonic/gin"
)

func setupV1(router *gin.Engine) {

	// routes for everyone, even non-logged in
	v1 := router.Group("/api/v1")
	{
		v1.GET("/version", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"version": models.Version,
				"type":    "v1",
				"build":   models.CommitNo,
				"date":    models.LastBuidDate,
				"domain":  c.Request.Host,
			})
		})
		v1.GET("/ping", controllers.Ping)
		v1.POST("/signup", controllers.Signup)
		v1.POST("/login", controllers.Login)
	}
	authorized := v1.Group("/")

	// routes for all logged in users
	authorized.Use(middlewares.Auth())
	authorized.Use(middlewares.Permission())
	{
		authorized.GET("/profile", controllers.GetProfile)
		authorized.GET("/user-level", controllers.GetUserLevel)

		// hospital routes
		authorized.POST("/hospital", controllers.CreateHospital)
		authorized.PUT("/hospital", controllers.UpdateHospital)
		authorized.DELETE("/hospital", controllers.DeleteHospital)

		// employee routes
		authorized.POST("/employee", controllers.CreateEmployee)
		authorized.PUT("/employee", controllers.UpdateEmployee)
		authorized.DELETE("/employee", controllers.DeleteEmployee)

		// patient routes
		authorized.POST("/patient", controllers.CreatePatient)
		authorized.PUT("/patient", controllers.UpdatePatient)
		authorized.DELETE("/patient", controllers.DeletePatient)

		// appointment routes
		authorized.POST("/appointment", controllers.CreateAppointment)
		authorized.PUT("/appointment", controllers.UpdateAppointment)
		authorized.DELETE("/appointment", controllers.DeleteAppointment)

		// diagnosis routes
		authorized.POST("/diagnosis", controllers.CreateDiagnosis)
		authorized.PUT("/diagnosis", controllers.UpdateDiagnosis)
		authorized.DELETE("/diagnosis", controllers.DeleteDiagnosis)

		// medicine routes
		authorized.POST("/medicine", controllers.CreateMedicine)
		authorized.PUT("/medicine", controllers.UpdateMedicine)
		authorized.DELETE("/medicine", controllers.DeleteMedicine)

		// prescription routes
		authorized.POST("/prescription", controllers.CreatePrescription)
		authorized.PUT("/prescription", controllers.UpdatePrescription)
		authorized.DELETE("/prescription", controllers.DeletePrescription)
	}
}
