package router

import (
	"net/http"

	"bitbucket.org/boolow5/HMS/fayo-api/middlewares"
	"github.com/gin-gonic/gin"
)

// Init initializes API routes, endpionts and middlware integrations
func Init() *gin.Engine {
	router := gin.Default()
	// set maximum file size
	router.MaxMultipartMemory = 1 << 20
	router.Use(middlewares.CORS())
	// router.Static("static", "/static")
	// router.StaticFile("/static", http.Dir("static"))
	router.StaticFS("/static", http.Dir("static"))
	setupV1(router)
	return router
}
