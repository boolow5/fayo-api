# Bolow-gin-boilerplate

After doing the same  initial setup everytime I decided to have this boiler plate for setting up new projects on Golang Gin framework.

### Setup

1. copy the or clone the repo in the path you want.
2. using your editor's find and replace feature, replace `bitbucket.org/boolow5/bolow-gin-boilerplate` with your own path.
3. config.json file contains all the setup for database and other external services
4. deploy.sh file contains info about the server you want to deploy, if you want to use `make deploy` to push your api to remote server
  1. change the IP (required)
  2. package name (optional)
  3. DEPLOY_PATH where you want the executable to be placed on remote server
  4. STATIC_PATH where you want the static files to be uploaded
  5. OWNER and WEB_USER are system users
  6. PROC_NAME is the process name for supervisord


### Dependencies

All dependencies are in vendor directory.
`govender sync`

### Database requirements:
This boilerplate uses Mongodb, for the API to run you need Mongodb running.

### Run

1. Using `go` to run use `go run main.go [directory-containing-the-config.json-file] `
2. Buld then run:

```
make build # will build for linux only, to change to other systems edit Makefile
./bin/bolow # if you don't change the executable name in `deploy.sh`
```